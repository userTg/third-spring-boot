package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;


@RunWith(SpringRunner.class)
@DataJpaTest
class UserRestControllerTest1 {
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testTestGetUsers() {
		User u1 = new User("Dalton", "Joe", "01/02/1980", "main", "12345", "Daisytown", 12345678);
		User u2 = new User("Luke", "JLucky", "15/06/1975", "wall", "198765", "PainfulGulch", 987654);
		
		entityManager.persist(u1);
		entityManager.persist(u2);
		List<User> allUsersFromDb = userRepository.findAll();
		List<User> userList = new ArrayList<>();
		for (User user : allUsersFromDb) {
			userList.add(user);
	}
	}

	@Test
	void testTestGetUserById() {
		User user = new User("Dalton", "Joe", "01/02/1980", "main", "12345", "Daisytown", 12345678);
		User userSavedInDb = entityManager.persist(user);
		User userFromDb = userRepository.getOne(userSavedInDb.getId());
		assertEquals(userSavedInDb, userFromDb);
		assertThat(userFromDb.equals(userSavedInDb));
	}

	@Test
	void testTestSave() {
		User u1 = new User("Dalton", "Joe", "01/02/1980", "main", "12345", "Daisytown", 12345678);
	User userSavedInDb = entityManager.persist(u1);
	User userFromDb = userRepository.getOne(userSavedInDb.getId());
	assertEquals(userSavedInDb, userFromDb);
	assertThat(userFromDb.equals(userSavedInDb));
	}

	@Test
	void testTestUpdateUser() {
		User user = new User("Luke", "JLucky", "15/06/1975", "wall", "198765", "PainfulGulch", 987654);
		entityManager.persist(user);
		User getFromDb = userRepository.getOne(user.getId());
		getFromDb.setNom("admino");
		assertThat(getFromDb.getNom().equals(user.getNom()));
	}

	@Test
	void testTestDeleteUser() {
		User u1 = new User("Dalton", "Joe", "01/02/1980", "main", "12345", "Daisytown", 12345678);
		User u2 = new User("Luke", "JLucky", "15/06/1975", "wall", "198765", "PainfulGulch", 987654);
		User persist = entityManager.persist(u1);
		entityManager.persist(u2);
		entityManager.remove(persist);
		List<User> allUsersFromDb = userRepository.findAll();
		List<User> userList = new ArrayList<>();
		for (User user : allUsersFromDb) {
			userList.add(user);
		}
		assertThat(userList.size()).isEqualTo(1);

	}
	}


