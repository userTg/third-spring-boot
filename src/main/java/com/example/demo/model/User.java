package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String prenom;
	private String dateAnniversaire;
	private String rue;
	private String codepostal;
	private String ville;
	private int telephone;
	
	
	
	public User() {
		super();
	}

	public User(String nom, String prenom, String dateAnniversaire, String rue, String codepostal, String ville,
			int telephone) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateAnniversaire = dateAnniversaire;
		this.rue = rue;
		this.codepostal = codepostal;
		this.ville = ville;
		this.telephone = telephone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getDateAnniversaire() {
		return dateAnniversaire;
	}

	public void setDateAnniversaire(String dateAnniversaire) {
		this.dateAnniversaire = dateAnniversaire;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getCodepostal() {
		return codepostal;
	}

	public void setCodepostal(String codepostal) {
		this.codepostal = codepostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateAnniversaire=" + dateAnniversaire
				+ ", rue=" + rue + ", codepostal=" + codepostal + ", ville=" + ville + ", telephone=" + telephone + "]";
	}
	
	
	
	

}
