package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exceptions.ResourceNotFoundException;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@RestController
public class UserrestController {
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping(value = "/users")
	public List<User> getUsers() {
		return userRepository.findAll();
	}
	
	@GetMapping(value = "/users/{id}")
	public ResponseEntity<User> getuserById(@PathVariable Integer id){
		User user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("user not found :: " + id));		
		return ResponseEntity.ok().body(user);
	}
	
	@PostMapping(value = "/users")
	public User save(@RequestBody User u) {
		return userRepository.save(u);
	}
	
	@PutMapping("/users/{id}")
    public ResponseEntity<User> updateUser(@RequestBody User u,
        @PathVariable(value = "id") Integer id) throws ResourceNotFoundException {
		User user = userRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("User not found :: " + id));
		user.setNom(u.getNom());
		user.setPrenom(u.getPrenom());
		user.setDateAnniversaire(u.getDateAnniversaire());
		user.setRue(u.getRue());
		user.setCodepostal(u.getCodepostal());
		user.setVille(u.getVille());
		user.setTelephone(u.getTelephone());
		
        final User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

}
